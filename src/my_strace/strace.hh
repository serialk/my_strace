#ifndef STRACE_HH
# define STRACE_HH

# include <sys/user.h>

# define ERROR(Val)                    \
    do {                               \
        std::cerr << Val << std::endl; \
        exit(1);                       \
    } while (0)                        \

void strace(char* filename, char** argv);

void print_syscall(int pid, user_regs_struct* regs);
void print_return(int pid, user_regs_struct* regs);

void print_type(int pid, long int value, int type);

#endif /* !STRACE_HH */
