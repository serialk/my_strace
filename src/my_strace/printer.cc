#include <iostream>
#include <cstdio>
#include <sys/ptrace.h>
#include "syscalls.hh"

void print_string(int pid, char* addr)
{
    union
    {
        char buf[5];
        int value;
    } buffer;

    fprintf(stderr, "\"");
    while (1)
    {
        buffer.value = (int) ptrace(PTRACE_PEEKTEXT, pid, addr, NULL);
        if (fprintf(stderr, "%s", buffer.buf) != 4)
            break;
        addr += 4;
    }
    fprintf(stderr, "\"");
}

void print_type(int pid, long int value, int type)
{
    if (type & PTR && type & CHAR)
        print_string(pid, (char*) value);
    else if (type & PTR)
    {
        if (!value)
            fprintf(stderr, "NULL");
        else
            fprintf(stderr, "%p", (void*) value);
    }
    else if (type & INT)
        fprintf(stderr, "%ld", value);
}
