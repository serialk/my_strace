#ifndef SYSCALLS_HH
# define SYSCALLS_HH

enum type_parameter
{
    VOID  = 0x0,
    CHAR  = 0x1,
    INT   = 0x2,
    PTR   = 0x4,
};

struct syscall_def
{
    int num;
    const char* name;
    int p1;
    int p2;
    int p3;
    int p4;
    int p5;
    int p6;
    int ret;
    const char* s1;
    const char* s2;
    const char* s3;
    const char* s4;
    const char* s5;
    const char* s6;
};

#endif /* !SYSCALLS_HH */
