#include <sys/user.h>
#include <iostream>
#include "syscalls.hh"
#include "strace.hh"

static const syscall_def syscalls[] =
{
# include "syscalls.def"
};

void print_syscall(int pid, user_regs_struct* regs)
{
    if (regs->orig_rax > sizeof (syscalls) / sizeof (syscalls[0]))
        return;
    syscall_def s = syscalls[regs->orig_rax];
    std::cerr << s.name << "(";
    bool first = true;
#define X(Type, Name, Register) \
    if (s.Type) \
    { \
        if (!first) \
            std::cerr << ", "; \
        else \
            first = false; \
        if (s.Name) \
            std::cerr << s.Name << " = "; \
        print_type(pid, regs->Register, s.Type); \
    }
#include "args.def"
#undef X
    std::cerr << ")";
}

void print_return(int pid, user_regs_struct* regs)
{
    if (regs->rax > sizeof (syscalls) / sizeof (syscalls[0]))
    {
        std::cerr << "\n";
        return;
    }
    syscall_def s = syscalls[regs->rax];
    if (s.ret != VOID)
    {
        std::cerr << " = ";
        print_type(pid, regs->rax, s.ret);
    }
    std::cerr << "\n";
}
