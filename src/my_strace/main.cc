#include <iostream>
#include "strace.hh"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " path/to/binary" << std::endl;
        return 1;
    }
    strace(argv[1], argv + 1);
    return 0;
}
