RM=rm -rf
CXX=g++

.PHONY: all clean distclean

all:
	mkdir -p build
	cd build && cmake ..
	$(MAKE) -s -C build

clean:
	$(RM) build
	$(RM) rt

distclean: clean
	$(RM) common.cmake
