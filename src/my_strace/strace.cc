#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <cstdlib>
#include "strace.hh"
#include <iostream>

user_regs_struct get_regs(int pid)
{
    user_regs_struct regs;
    if (ptrace(PTRACE_GETREGS, pid, NULL, &regs) == -1)
        ERROR("PTRACE_GETREGS failed");
    return regs;
}

void trace_syscalls(int pid)
{
    int ret;
    bool returning = false;
    wait4(pid, &ret, 0, NULL);
    if (WIFEXITED(ret))
        return;
    while (1)
    {
        if (ptrace(PTRACE_SYSCALL, pid, NULL, NULL) == -1)
            ERROR("PTRACE_SYSCALL failed");
        wait4(pid, &ret, 0, NULL);
        if (WIFEXITED(ret))
            return;
        user_regs_struct regs = get_regs(pid);
        if (!returning)
        {
            short v = ptrace(PTRACE_PEEKTEXT, pid, regs.rip, NULL);
            if (v == -1)
                ERROR("PTRACE_PEEKTEXT failed");
            if (1)//v == 0x050f)
            {
                returning = true;
                print_syscall(pid, &regs);
            }
        }
        else
        {
            print_return(pid, &regs);
            returning = false;
        }
    }
}

int exec(char* filename, char** argv)
{
    int pid = fork();
    if (!pid)
    {
        if (ptrace(PTRACE_TRACEME, getpid(), NULL, NULL) == -1)
            ERROR("PTRACE_TRACEME failed");
        if (execvp(filename, argv) == -1)
            ERROR("Cannot execvp");
    }
    return pid;
}

void strace(char* filename, char** argv)
{
    int pid = exec(filename, argv);
    if (pid == -1)
        exit(1);
    trace_syscalls(pid);
    std::cerr << "\n";
}
